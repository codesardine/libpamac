const string VERSION = "11.2.0";

namespace Pamac {
	public string get_version () {
		return VERSION;
	}
}
